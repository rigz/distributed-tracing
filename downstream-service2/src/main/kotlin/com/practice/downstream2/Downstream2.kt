package com.practice.downstream2

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Downstream2

fun main(args: Array<String>) {
  runApplication<Downstream2>(*args)
}
