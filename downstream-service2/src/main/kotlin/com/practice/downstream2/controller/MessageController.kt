package com.practice.downstream2.controller

import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDateTime

@RestController
@RequestMapping("downstream-service")
class MessageController {
  companion object {
    val logger = LoggerFactory.getLogger(MessageController::class.java)
  }

  @GetMapping("message")
  fun getMessage(@RequestHeader headers: Map<String,String>): String {
    for (header in headers) {
      logger.info("Header-->[${header.key}, ${header.value}]")
    }
    logger.info("Returning message from downstream service 2 at ${LocalDateTime.now()}")
    return "Hello World from downstream Service 2! ${LocalDateTime.now()}";
  }
}
