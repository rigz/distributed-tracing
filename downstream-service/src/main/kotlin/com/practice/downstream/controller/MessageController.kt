package com.practice.downstream.controller

import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.client.RestTemplate
import org.springframework.web.util.UriComponentsBuilder
import java.time.LocalDateTime

@RestController
@RequestMapping("downstream-service")
class MessageController (val restTemplate: RestTemplate) {

  companion object {
    val logger = LoggerFactory.getLogger(MessageController::class.java)
  }

  @GetMapping("message")
  fun getMessage(@RequestHeader headers: Map<String,String>): String {
    for (header in headers) {
      logger.info("Header-->[${header.key}, ${header.value}]")
    }
    logger.info("Returning message from downstream service 1 at ${LocalDateTime.now()}")
    return "Hello World from downstream service 1! ${LocalDateTime.now()}";
  }
  @GetMapping("message2")
  fun getMessage2(@RequestHeader headers: Map<String,String>): String {
    for (header in headers) {
      logger.info("Header-->[${header.key}, ${header.value}]")
    }
    logger.info("Calling downstream downstream service 2")
    val result: ResponseEntity<String> = restTemplate.getForEntity(
      UriComponentsBuilder
        .fromHttpUrl("http://localhost:8020/downstream-service/message").build().toUri(),
      String::class.java
    )
    return result.body?.toString() ?:"No message returned" ;
  }
}
