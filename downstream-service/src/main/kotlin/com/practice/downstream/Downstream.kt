package com.practice.downstream

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Downstream

fun main(args: Array<String>) {
  runApplication<Downstream>(*args)
}
