## Micronaut 3.5.2 Documentation

- [User Guide](https://docs.micronaut.io/3.5.2/guide/index.html)
- [API Reference](https://docs.micronaut.io/3.5.2/api/index.html)
- [Configuration Reference](https://docs.micronaut.io/3.5.2/guide/configurationreference.html)
- [Micronaut Guides](https://guides.micronaut.io/index.html)

---

## Feature tracing-jaeger documentation

- [Micronaut Jaeger Tracing documentation](https://micronaut-projects.github.io/micronaut-tracing/latest/guide/#jaeger)

- [https://www.jaegertracing.io/](https://www.jaegertracing.io/)

## Feature http-client documentation

- [Micronaut HTTP Client documentation](https://docs.micronaut.io/latest/guide/index.html#httpClient)


