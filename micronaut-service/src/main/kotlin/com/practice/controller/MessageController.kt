package com.practice.controller

import com.practice.client.DownstreamClient
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import jakarta.inject.Inject
import org.slf4j.LoggerFactory

@Controller("/micronaut-service")
open class MessageController( @Inject private val downstreamClient: DownstreamClient) {
  companion object{
    val logger by lazy { LoggerFactory.getLogger(MessageController::class.java) }
  }

  @Get("/message")
  fun getMessage( ): String {
    logger.info("Micronaut getting message from downstream 1")
    return downstreamClient.getMessage()
  }
  @Get("/message2")
  fun getMessage2( ): String {
    logger.info("Micronaut getting message from downstream 2")
    return downstreamClient.getMessage2()
  }
  @Get("/hello")
  fun getMessageFromLocal(): String {
    logger.info("Micronaut sending message")
    return "Hello World from Micronaut!"
  }

}
