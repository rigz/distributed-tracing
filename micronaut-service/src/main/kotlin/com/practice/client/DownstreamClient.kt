package com.practice.client

import io.micronaut.http.annotation.Get
import io.micronaut.http.client.annotation.Client

@Client("http://localhost:8010/downstream-service")
abstract class DownstreamClient {
  @Get("message")
  abstract fun getMessage():String
  @Get("message2")
  abstract fun getMessage2(): String
}
