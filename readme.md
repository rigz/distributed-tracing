- **spring-service** : it mimics a kotlin spring boot application. http://localhost:8000/spring-service/message
- **micronaut-service** : it mimics a kotlin micronaut application. http://localhost:8001/micronaut-service/message
- **dropwizard-service** : it mimics a java dropwizard server application http://localhost:8002/dropwizard-service/message
- **downstream-service** : server app called by above 3 as downstream http://localhost:8010/downstream-service/message  and http://localhost:8010/downstream-service/message2 to call another downstream 
- **downstream-service2** : server app called by downstream service. This mimics another endpoint being called from a downstream http://localhost:8010/downstream-service2/message


