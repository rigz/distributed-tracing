package com.practice.springService

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SpringService

fun main(args: Array<String>) {
  runApplication<SpringService>(*args)
}
