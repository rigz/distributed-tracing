package com.practice.springService.controller

import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.client.RestTemplate
import org.springframework.web.util.UriComponentsBuilder

@RestController
@RequestMapping("/spring-service")
class MessageController(val restTemplate: RestTemplate) {
  companion object {
    private val logger = LoggerFactory.getLogger(MessageController::class.java)
  }
  @GetMapping("/message")
  fun getMessage(@RequestHeader headers: Map<String,String>): String {
    for (header in headers) {
      logger.info("Header-->[${header.key}, ${header.value}]")
    }
    logger.info("Spring boot getting message from downstream 1")
    val result: ResponseEntity<String> = restTemplate.getForEntity(
      UriComponentsBuilder
        .fromHttpUrl("http://localhost:8010/downstream-service/message").build().toUri(),
      String::class.java
    )
    return result.body?.toString() ?:"No message returned" ;
  }
  @GetMapping("/message2")
  fun getMessage2(@RequestHeader headers: Map<String,String>): String {
    for (header in headers) {
      logger.info("Header-->[${header.key}, ${header.value}]")
    }
    logger.info("Spring boot getting message from downstream 2")
    val result: ResponseEntity<String> = restTemplate.getForEntity(
      UriComponentsBuilder
        .fromHttpUrl("http://localhost:8010/downstream-service/message2").build().toUri(),
      String::class.java
    )
    return result.body?.toString() ?:"No message returned" ;
  }
  @GetMapping("/hello")
  fun get(): String {
    logger.info("Spring sending message")
    return "Hello world from service 1"
  }


}
