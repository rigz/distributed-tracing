package com.practice;

import javax.ws.rs.client.Invocation;
import java.util.Iterator;
import java.util.Map;

public class RequestMapBuilderJersey implements io.opentracing.propagation.TextMap {
  private final Invocation.Builder builder;

  public RequestMapBuilderJersey(Invocation.Builder builder) {
    this.builder = builder;
  }

  @Override
  public Iterator<Map.Entry<String, String>> iterator() {
    throw new UnsupportedOperationException("carrier is write-only");

  }

  @Override
  public void put(String key, String value) {
    builder.header(key,value);
  }
}
