package com.practice;

import com.practice.resources.MessageResource;
import io.dropwizard.Application;
import io.dropwizard.setup.Environment;
import io.opentracing.Tracer;

public class DwApplication extends Application<DwApplicationConfiguration> {
  private final Tracer tracer;

  public DwApplication(Tracer tracer) {
    this.tracer = tracer;
  }

  public static void main(final String[] args) throws Exception {
    System.setProperty("dw.server.applicationConnectors[0].port", "8002");
    System.setProperty("dw.server.adminConnectors[0].port", "9081");
        new DwApplication(Tracing.init("dw-application")).run(args);
    }



    @Override
    public void run(final DwApplicationConfiguration configuration,
                    final Environment environment) {
        environment.jersey().register(new MessageResource(tracer));
    }



}
