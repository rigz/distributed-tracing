package com.practice.resources;

import com.codahale.metrics.annotation.Timed;
import com.practice.RequestMapBuilderOkHttp;
import com.practice.Tracing;
import io.opentracing.Scope;
import io.opentracing.Span;
import io.opentracing.Tracer;
import io.opentracing.log.Fields;
import io.opentracing.propagation.Format;
import io.opentracing.tag.Tags;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Response;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.Map;

@Path("/dropwizard-service")
@Produces(MediaType.APPLICATION_JSON)
public class MessageResource {

  private final OkHttpClient client;
  private Tracer tracer;


  public MessageResource(Tracer tracer) {
    client = new OkHttpClient();
    this.tracer = tracer;
  }

  @GET
  @Timed
  @Path("/message")
  public String getMessage(@Context HttpHeaders httpHeaders) {
    Span span = Tracing.startServerSpan(tracer, httpHeaders, "format");
    try (Scope scope = tracer.scopeManager().activate(span)) {
      String result = getHttp();
      span.log(Map.of("result",result));
      return result;
    } finally {
      span.finish();
    }
  }


  private String getHttp() {
    try {
      HttpUrl url = new HttpUrl.Builder()
        .scheme("http")
        .host("localhost")
        .port(8010)
        .addPathSegment("downstream-service")
        .addPathSegment("message2")
        .build();
      okhttp3.Request.Builder requestBuilder = new okhttp3.Request.Builder().url(url);

      Span activeSpan = tracer.activeSpan();
      Tags.SPAN_KIND.set(activeSpan, Tags.SPAN_KIND_CLIENT);
//      Tags.HTTP_METHOD.set(activeSpan, "GET");
      Tags.HTTP_URL.set(activeSpan, url.toString());
      tracer.inject(activeSpan.context(), Format.Builtin.HTTP_HEADERS, new RequestMapBuilderOkHttp(requestBuilder));


      okhttp3.Request request = requestBuilder.build();
      Response response = client.newCall(request).execute();

      Tags.HTTP_STATUS.set(tracer.activeSpan(), response.code());
      if (response.code() != 200) {
        throw new RuntimeException("Bad HTTP result: " + response);
      }
      return response.body().string();
    } catch (IOException e) {
      Tags.ERROR.set(tracer.activeSpan(), true);
      tracer.activeSpan().log(Map.of(Fields.EVENT, "error", Fields.ERROR_OBJECT, e));
      throw new RuntimeException(e);
    }
  }

  @GET
  @Timed
  @Path("/hello")
  public String get() {
    return "Hello World from Dropwizard";
  }
}
